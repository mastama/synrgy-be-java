package week1_homework;

import java.util.Scanner;

public class Investasi {
    public static void main(String[] args) {

        Scanner inputInvest = new Scanner(System.in);
        double p = investDouble(inputInvest, "dana awal");
        double r = investDouble(inputInvest, "suku bunga");
        int n = investInt(inputInvest, "berapa kali bunga diterapkan per periode");
        int t = investInt(inputInvest, "jumlah periode waktu yang berlalu");

        /*

        System.out.print("Enter suku bunga: ");
        double r = inputInvest.nextDouble();

        System.out.print("Enter berapa kali bunga diterapkan per periode waktu: ");
        int n = inputInvest.nextInt();

        System.out.print("Enter jumlah periode waktu yang berlalu: ");
        int t = inputInvest.nextInt();
         */

        double A = p * Math.pow((1 + r / n), t * n);
        // System.out.println("Uang yang diterima= " + A);
        printHasil(A);
    }

    private static void printHasil(double hasil) {
        System.out.println("Uang uang diterima: " + hasil);
    }

    private static double investDouble(Scanner inputInvest, String text) {
        System.out.print("Enter " + text + ": ");
        return inputInvest.nextDouble();
    }

    private static int investInt(Scanner inputInvest, String text) {
        System.out.print("Enter " + text + ": ");
        return inputInvest.nextInt();
    }
}
