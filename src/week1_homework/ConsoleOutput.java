package week1_homework;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ConsoleOutput {
    public static void main(String[] args) throws IOException {

        String path = "D:\\Im EXPERT of SOFTWARE ENGINEER\\synrgy\\Java\\exerciseJava\\src\\week1_homework\\ConsoleOutput";
        Scanner bio = new Scanner(System.in);

        String nama = biodata(bio, "nama");
        String alamat = biodata(bio, "alamat");
        int usia = Integer.parseInt(biodata(bio, "usia"));
        char kuliah = biodata(bio, "sudah lulus kuliah (y/n)").charAt(0);

        PrintWriter output = new PrintWriter(new FileWriter(path.concat("biodata.txt")));
        output.println(nama);
        output.println(alamat);
        output.println(usia);
        output.print(kuliah);

        output.close();
    }

    private static String biodata(Scanner bio, String text) {
        System.out.println("Enter " + text + " :");
        return bio.nextLine();
    }
}
