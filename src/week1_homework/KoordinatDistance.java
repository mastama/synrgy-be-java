package week1_homework;

import java.util.Scanner;

public class KoordinatDistance {
    public static void main(String[] args) {

        Scanner inputKoor = new Scanner(System.in);

        // menggunakan method
        int x1 = koordinat(inputKoor, "x1");
        int y1 = koordinat(inputKoor, "y1");
        int x2 = koordinat(inputKoor, "x2");
        int y2 = koordinat(inputKoor, "y2");

        //menggunakan cara biasa
        /*
        System.out.print("Enter x1: ");
        int x1 = inputKoor.nextInt();

        System.out.print("Enter y1: ");
        int y1 = inputKoor.nextInt();

        System.out.print("Enter x2: ");
        int x2 = inputKoor.nextInt();

        System.out.print("Enter y2: ");
        int y2 = inputKoor.nextInt();
         */

        double jarak = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        printHasil(x1, y1, x2, y2, jarak);

        // System.out.println("Jarak antara (" + x1 + ", " + y1 + ") dan (" + x2 + ", " + y2 + ") = " + jarak);
    }

    public static void printHasil(int x1, int y1, int x2, int y2, double jarak) {
        System.out.println("Jarak antara (" + x1 + ", " + y1 + ") dan (" + x2 + ", " + y2 + ") = " + jarak);
    }

    private static int koordinat(Scanner inputKoor, String koordinat) {
        System.out.print("Enter " + koordinat + ":");
        int x1 = inputKoor.nextInt();
        return x1;
    }
}
