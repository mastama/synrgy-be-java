package week1_homework;

public class Playground {
    public static void main(String[] args) {

        String name = "abcdefghijklmnopqrstuvwxyz";
        System.out.println("name length: " + name.length());
        System.out.println("name charAt: " + name.charAt(22));

        char a = name.charAt(0);
        char b = name.charAt(21);
        System.out.print("ab = " + a + b);

    }
}
