package week1;

import java.util.Scanner;

public class Introduction {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter name: ");
        String name = keyboard.nextLine();

        System.out.println("Enter age: ");
        int age = Integer.parseInt(keyboard.nextLine());
        //keyboard.nextLine();

        System.out.println("Enter address: ");
        String address = keyboard.nextLine();

        System.out.println("IPK GPA: ");
        Double gpa = Double.parseDouble(keyboard.nextLine());
        //keyboard.nextLine();

        System.out.println("Are you married (y/n): ");
        String status = keyboard.next();
        keyboard.close();

        System.out.println("------- Biodata -------");
        System.out.println("Nama saya: " + name);
        System.out.println("Usia saya: " + age);
        System.out.println("Alamat saya: " + address);
        System.out.println("IPK saya: " + gpa);
        System.out.println("Sudah menikah: " + status.toLowerCase());

    }
}
