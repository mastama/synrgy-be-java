package week1;

import java.util.Locale;

public class StringPlayground {
    public static void main(String[] args) {

        String name = "             SingGih PraTama            ";
        String fruit = "Apple";

        //Java String Built-in Method
        System.out.println("Nama saya: " + name.trim());
        System.out.println("Nama saya panjanganya: " + name.trim().length());

        System.out.println("Saya suka: ".concat(fruit));

        System.out.println(name.toLowerCase().trim());
    }
}
