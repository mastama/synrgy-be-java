package week1;

public class ExampleMethod {
    public static void main(String[] args) {

        double nilaiUjian = printReturnDouble(50);
        System.out.println("Ujian saya dapat: " + nilaiUjian);

        double nilaiUjian2orang = printReturnDouble(10, 20);
        System.out.println("Ujian 2 orang dapat: " + nilaiUjian2orang);

        // method invoke mean memanggil method
    }

    private static double printReturnDouble(double x) {
        return x;
    }

    private static int printReturnDouble(int x, int y) {
        return x + y;
    }
}
