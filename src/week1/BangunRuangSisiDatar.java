package week1;

import java.util.Scanner;

public class BangunRuangSisiDatar {
    public static void main(String[] args) {
        Scanner math = new Scanner(System.in);

        System.out.println("Masukkan sisi persegi: ");
        double sisi = math.nextDouble();


    }

    private static double luasPersegiPanjang(double p, double l) {
        return p * l;
    }

    private static double kelilingPersegiPanjang(double p, double l) {
        return (2 * p) + (2 * l);
    }

    private static double luasPersegi(double p) {
        return Math.pow(p, 2);
    }

    private static double kelilingPersegi(double p) {
        return 4 * p;
    }

    private static double luasLingkaran(double r) {
        return Math.round(Math.PI * Math.pow(r, 2) * 100.) / 100.;
    }

    private static double kelilingLingkaran(double r) {
        return Math.round(2 * Math.PI * r * 100.) / 100;
    }
}
