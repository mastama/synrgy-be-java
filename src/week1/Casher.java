package week1;

import java.util.Scanner;

public class Casher {
    public static void main(String[] args) {

        Scanner casher = new Scanner(System.in);
        double beef = 75000;
        double mutton = 90000;
        double chicken = 35000;
        double duck = 50000;

        System.out.print("Porsi beef yang dipesan: ");
        int jumlahBeef = casher.nextInt();

        System.out.print("Porsi mutton yang dipesan: ");
        int jumlahMutton = casher.nextInt();

        System.out.print("Porsi chicken yang dipesan: ");
        int jumlahChicken = casher.nextInt();

        System.out.print("Porsi duck yang dipesan: ");
        int jumlahDuck = casher.nextInt();

        double total = (jumlahBeef * beef) + (jumlahMutton * mutton) + (jumlahChicken * chicken) + (jumlahDuck * duck);
        System.out.println("TOtal harga yang harus dibayar: Rp." + total);
    }
}
