package week1;

import java.util.Scanner;

public class Gradient {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter nama: ");
        String name = keyboard.nextLine();
        System.out.println("nama: " + name);
// ------------------------------------------------------------
        System.out.println("Enter x1: ");
        double x1 = keyboard.nextDouble();

        System.out.println("Enter x2: ");
        double x2 = keyboard.nextDouble();

        System.out.println("Enter y1: ");
        double y1 = keyboard.nextDouble();

        System.out.println("Enter y2: ");
        double y2 = keyboard.nextDouble();

        double gradient = (y2 - y1) / (x2 - x1);
        System.out.println("Gradient = " + gradient);

    }
}
