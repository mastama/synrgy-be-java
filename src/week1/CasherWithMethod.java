package week1;

import java.util.Scanner;

public class CasherWithMethod {
    public static void main(String[] args) {

        Scanner casherWithMethod = new Scanner(System.in);
        double beef = 75000;
        double mutton = 90000;
        double chicken = 35000;
        double duck = 50000;

        int jumlahBeef = jumlahMakanan(casherWithMethod, "daging sapi");
        int jumlahMutton = jumlahMakanan(casherWithMethod, "daging kambing");
        int jumlahChicken = jumlahMakanan(casherWithMethod, "daging ayam");
        int jumlahDuck = jumlahMakanan(casherWithMethod, "daging bebek");


        double total = (jumlahBeef * beef) + (jumlahMutton * mutton) + (jumlahChicken * chicken) + (jumlahDuck * duck);
        System.out.println("Total harga yang harus dibayar: Rp." + total);
    }

    private static int jumlahMakanan(Scanner casherWithMethod, String jenisMakanan) {
        System.out.print("Porsi ".concat(jenisMakanan) + " yang dipesan: ");
        int jumlahBeef = casherWithMethod.nextInt();
        return jumlahBeef;
    }
}
