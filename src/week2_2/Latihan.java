package week2_2;

import java.text.ParseException;
import java.util.Scanner;

public class Latihan {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        /*
        // membuat app java dengan memprint text1 ke text2 dan text2 ke text1
        System.out.print("Enter text 1: ");
        String text1 = kb.nextLine();

        System.out.print("Enter text 2: ");
        String text2 = kb.nextLine();

        System.out.println(text1.concat(text2));
        System.out.println(text2.concat(text1));

         */

        /*
        // membuat app java dengan tiap nomor dapat dijumlah dan menampilkan hasilnya
        System.out.print("Masukkan nomor: ");
        String nomor = kb.nextLine();

        int jumlah = 0;

        for (int i=0; i<nomor.length(); i++ ) {
            jumlah += nomor.charAt(i)-48;
        }
        System.out.println("Jummlah = " + jumlah);

         */
        // membuat app java untuk bisa menghitung jumlah angka genap dan jumlah angka ganjil
        System.out.print("Masukkan nomor: ");
        String nomor = kb.nextLine();

        int jumlahGenap = 0;
        int jumlahGanjil = 0;

        for (int i=0; i<nomor.length(); i++) {
            int angka = nomor.charAt(i) - 48;

            if (angka % 2 == 0) {
                jumlahGenap += angka;
            }else {
                jumlahGanjil += angka;
            }
        }

        System.out.println("Jumlah nomor genap: " + jumlahGenap);
        System.out.println("Jumlah nomor ganjil: " + jumlahGanjil);
    }
}
