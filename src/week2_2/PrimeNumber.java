package week2_2;

public class PrimeNumber {
    public static void main(String[] args) {
        //print bilangan prima sampai 1000

        for (int i=2; i<1000; i++) {
            if (isPrimeNumber(i)) {
                System.out.print(i + " ");
            }
        }
    }

    private static boolean isPrimeNumber(int number) {
        for (int i=2; i<=number; i++) {
            if(number % i == 0) {
                return false;
            }
        }
        return true;
    }


}
