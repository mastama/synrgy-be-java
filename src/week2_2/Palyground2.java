package week2_2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Palyground2 {
    public static void main(String[] args) throws IOException {
        // Cara membaca sebuah file menggunakan while loop
        /*
        String path = "D:/Im EXPERT of SOFTWARE ENGINEER/synrgy/Java/exerciseJava/src/week2_2";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path.concat("test.txt")));

        String baris = bufferedReader.readLine();
        while(baris != null) {
            System.out.println(baris);

            baris = bufferedReader.readLine();
        }



        for (int i=1; i<10; i++) {
            for (int j=1; j<10; j++) {
                System.out.println(i + "*" + j + " = " + i*j);
            }
        }
         */

        for (int i=1; i<10; i++) {
            for (int j=1; j<i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i=1; i<10; i++) {
            for (int j=i; j<10; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
