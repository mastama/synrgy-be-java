package week2_2;

import java.util.Scanner;

public class Playground {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        System.out.print("Masukan nomor: ");
        int n = kb.nextInt();

        do {
            System.out.println("n = " + n);
            System.out.println("Masukan nomor (dalam do while): ");
            n = kb.nextInt();
        }while (n != 0);
        System.out.println("keluar dari do while loop");

        while(n != 0) {
            System.out.println("n = " + n);

            System.out.println("Masukan nomor (dalam while): ");
            n = kb.nextInt();

            System.out.println("Masukan nomor (dalam while): ");
        }
    }
}
