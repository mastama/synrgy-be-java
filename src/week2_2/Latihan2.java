package week2_2;

import java.util.Arrays;

public class Latihan2 {
    public static void main(String[] args) {
        int[] arr = {100, 20, 15, 30, 5, 75, 40};

        Arrays.sort(arr);
        System.out.println("Sorted arrays in ascending: " + Arrays.toString(arr));
    }
}
