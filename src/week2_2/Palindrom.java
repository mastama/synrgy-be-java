package week2_2;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.print("Masukkan kata: ");
        String kata = kb.nextLine();

        boolean palindrom = true;

        for (int i=0; i<kata.length()/2; i++) {
            if (kata.charAt(i) != kata.charAt(kata.length()-1-i)) {
                System.out.println(kata + " bukan palindrome");
                palindrom = false;
                break;
            }
        }

        if (palindrom == true) {
            System.out.println(kata + " adalah palindrome");
        }

    }
}
