package week2_3;

public class Utama {
   public static void main(String[] args) {

      // Nasabah = ini adalah constructor
      //Nasabah nasabah0 = new Nasabah(); // no argument/parameter constructor
      Nasabah nasabah1 = new Nasabah("Pratama", 22);
      Nasabah nasabah2 = new Nasabah("Mastama");
      Nasabah nasabah3 = new Nasabah("Jangkrik", 1);
      Nasabah nasabah4 = new Nasabah("casper", 200);
      Nasabah nasabahBaru = nasabah3;

      System.out.println("nasabah1.getName = " + nasabah1.getName());
      System.out.println("nasabah1.getAge = " + nasabah1.getAge());

      System.out.println("nasabah2.getName = " + nasabah2.getName());

      nasabah4.setName("Masjih");
      nasabah4.setAge(22);
      System.out.println("nasabah4.getName = " + nasabah4.getName());
      System.out.println("nasabah4.getAge = " + nasabah4.getAge());

      Nasabah nasabah5 = new Nasabah();
      nasabah5.setAge(80);
      System.out.println("nasabah5 nama = " + nasabah5.getName());
      System.out.println("nasabah5 usia = " + nasabah5.getAge());

      Nasabah nasabah6 = new Nasabah();
      nasabah6.setAge(2000);
      nasabah6.setName("Casper");
      System.out.println("nasabah6 name = " + nasabah5.getName());
      System.out.println("nasabah6 age = " + nasabah5.getAge());
   }
}
