package week2_3;

public class Nasabah {
   //field nya class constructor Nasabah
   private String name;
   private int age;

   //membuat constructor

   public Nasabah() {
   }

   public Nasabah(String name, int age) {
      this.name = name;
      this.age = age;
   }

   public Nasabah(String name) {
      this.name = name;
   }
   public Nasabah(int age) {
      this.age = age;
   }

   //membuat getter dan setter (Encapsulations)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public int getAge() {
      return age;
   }

   public void setAge(int age) {
      this.age = age;
   }

   // membuat method lainnya
   public void printNameAge() {
      System.out.println("Nama lengkap: " + this.name);
      System.out.println("Usia: " + this.age);
   }

   @Override
   public String toString() {
      return "Nasabah{" +
              "name='" + name + '\'' +
              ", age=" + age +
              '}';
   }
}
