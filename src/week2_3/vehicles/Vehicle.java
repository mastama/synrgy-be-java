package week2_3.vehicles;

public class Vehicle {
   private String type;

   //membuat constructor
   public Vehicle() {
   } // default constructor

   public Vehicle(String type) {
      this.type = type;
   }

   //membuat getter dan setter
   public String getType() {
      return type;
   }

   public void setType(String type) {
      this.type = type;
   }

   public void printVehicle() {
      System.out.println("Kendaraan ini berasal dari Vehicle.java");
   }
}
