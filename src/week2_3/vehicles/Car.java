package week2_3.vehicles;

public class Car extends Vehicle{
   private String merk;
   private int displacement;

   // membuat constructor
   public Car() {
   }

   public Car(String merk, int displacement) {
      this.merk = merk;
      this.displacement = displacement;
   }

   //membuat getter dan setter

   public String getMerk() {
      return merk;
   }

   public void setMerk(String merk) {
      this.merk = merk;
   }

   public int getDisplacement() {
      return displacement;
   }

   public void setDisplacement(int displacement) {
      this.displacement = displacement;
   }

   //membuat method
   public void printVehicle() {
      System.out.println("kendaraan ini berasal dari Car.java");
   }
}
