package week2_3.vehicles;

public class Utama {
   public static void main(String[] args) {

      // menerapkan konsep Polimorphism
      Vehicle vehicle = new Vehicle();
      Vehicle car = new Car("toyota", 5000);

      vehicle.printVehicle();
      car.printVehicle();
   }
}
