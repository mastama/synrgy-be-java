package week2_4;

public interface School {
   // penggunaan interface tidak memiliki body atau implementasinya

   public String namaSekolah();
   public void printSiswa();
   public String getStudent();
}
