package week2_4;

public class Smu1 implements School {
   private String nama;

   @Override
   public String namaSekolah() {
      return "SMU 1";
   }

   @Override
   public void printSiswa() {
      System.out.println("Siswa di SMU1");
   }

   @Override
   public String getStudent() {
      return this.nama;
   }
}
