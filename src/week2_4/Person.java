package week2_4;

public abstract class Person {
   //membuat field
   protected String nama;

   public abstract void kerjaUjian();
   //penggunaan abstract method tidak boleh ada pengimplementasiannya {}

   public abstract String printNama();

   public void kerjaTugas() {
      System.out.println("Kerja tugas");
   }

   public String getNama() {
      return nama;
   }

   public void setNama(String nama) {
      this.nama = nama;
   }
}
