package week2_4.inheritance;

public class Harga {
   // membuat field
   protected double hargaTanpaPajak;

   // membuat constructor
   // constructor no arguments/params atau default constructor
   public Harga() {
   }

   // constructor dengan satu argument
   public Harga(double hargaTanpaPajak) {
      this.hargaTanpaPajak = hargaTanpaPajak;
   }

   //getter
   public double getHargaTanpaPajak() {
      return hargaTanpaPajak;
   }

   //setter
   public void setHargaTanpaPajak(double hargaTanpaPajak) {
      this.hargaTanpaPajak = hargaTanpaPajak;
   }

   // toString
   public String toString() {
      return "hargaTanpaPajak = " + hargaTanpaPajak;
   }

   //membuat method
   public double hargaBarang() {
      return this.hargaTanpaPajak;
   }
}
