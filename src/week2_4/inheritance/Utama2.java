package week2_4.inheritance;

public class Utama2 {
   public static void main(String[] args) {

      //membuat variable array []
      Harga[] benda = new Harga[4];

      benda[0] = new Harga(250);
      benda[1] = new Harga(375);
      benda[2] = new HargaDenganPajak(250);
      benda[3] = new HargaDenganPajak(375);

      for (int i = 0; i< benda.length; i++) {
         System.out.println("benda[" + i + "] = " + benda[i].hargaBarang());
      }
   }
}
