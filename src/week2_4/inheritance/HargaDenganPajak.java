package week2_4.inheritance;

public class HargaDenganPajak extends Harga{
   //tidak perlu mmebuat field lagi

   //membuat constructor
   public HargaDenganPajak(double harga) {
      super(harga);
   }

   //membuat method
   public double hargaBarang() {
      double pajak = 10/100. * hargaTanpaPajak;
      return hargaTanpaPajak + pajak;
   }
}
