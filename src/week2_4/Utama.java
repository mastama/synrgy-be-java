package week2_4;

public class Utama {
   public static void main(String[] args) {

      // abstract class
      Person siswa = new Siswa();
      Person guru = new Guru();

      siswa.setNama("Pratama");
      System.out.println("siswa.getNama: " + siswa.getNama());

      // interface
      School smu1 = new Smu1();
      System.out.println(smu1.namaSekolah());
      smu1.printSiswa();
   }
}
