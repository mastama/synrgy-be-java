package week2_4;

public interface University {
   public void printUniversity();
}

interface ITB extends University {
   public void printMuridItb();
}
