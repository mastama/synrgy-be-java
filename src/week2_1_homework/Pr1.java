package week2_1_homework;

public class Pr1 {
    public static void main(String[] args) {

        String name = "Nama saya Ronaldo";

        String[] alphabet = {"a", "b", "c", "d", "e",
                "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o",
                "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z"
        };

        int[] sum = new int[alphabet.length];

        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == 'a' || name.charAt(i) == 'A') {
                sum[0]++;
            }else if (name.charAt(i) == 'b' || name.charAt(i) == 'B') {
                sum[1]++;
            }else if (name.charAt(i) == 'c' || name.charAt(i) == 'C') {
                sum[2]++;
            }else if (name.charAt(i) == 'd' || name.charAt(i) == 'D') {
                sum[3]++;
            }else if (name.charAt(i) == 'e' || name.charAt(i) == 'E') {
                sum[4]++;
            }else if (name.charAt(i) == 'f' || name.charAt(i) == 'F') {
                sum[5]++;
            }else if (name.charAt(i) == 'g' || name.charAt(i) == 'G') {
                sum[6]++;
            }else if (name.charAt(i) == 'h' || name.charAt(i) == 'H') {
                sum[7]++;
            }else if (name.charAt(i) == 'i' || name.charAt(i) == 'I') {
                sum[8]++;
            }else if (name.charAt(i) == 'j' || name.charAt(i) == 'J') {
                sum[9]++;
            }else if (name.charAt(i) == 'k' || name.charAt(i) == 'K') {
                sum[10]++;
            }else if (name.charAt(i) == 'l' || name.charAt(i) == 'L') {
                sum[11]++;
            }else if (name.charAt(i) == 'm' || name.charAt(i) == 'M') {
                sum[12]++;
            }else if (name.charAt(i) == 'n' || name.charAt(i) == 'N') {
                sum[13]++;
            }else if (name.charAt(i) == 'o' || name.charAt(i) == 'O') {
                sum[14]++;
            }else if (name.charAt(i) == 'p' || name.charAt(i) == 'P') {
                sum[15]++;
            }else if (name.charAt(i) == 'q' || name.charAt(i) == 'Q') {
                sum[16]++;
            }else if (name.charAt(i) == 'r' || name.charAt(i) == 'R') {
                sum[17]++;
            }else if (name.charAt(i) == 's' || name.charAt(i) == 'S') {
                sum[18]++;
            }else if (name.charAt(i) == 't' || name.charAt(i) == 'T') {
                sum[19]++;
            }else if (name.charAt(i) == 'u' || name.charAt(i) == 'U') {
                sum[20]++;
            }else if (name.charAt(i) == 'v' || name.charAt(i) == 'V') {
                sum[21]++;
            }else if (name.charAt(i) == 'w' || name.charAt(i) == 'W') {
                sum[22]++;
            }else if (name.charAt(i) == 'x' || name.charAt(i) == 'X') {
                sum[23]++;
            }else if (name.charAt(i) == 'y' || name.charAt(i) == 'Y') {
                sum[24]++;
            }else if (name.charAt(i) == 'z' || name.charAt(i) == 'Z') {
                sum[25]++;
            }
        }

        for (int i = 0; i < alphabet.length; i++) {
            if (sum[i] == 0) continue;

            System.out.println(alphabet[i] + " muncul " + sum[i] + " kali");
        }
    }
}
