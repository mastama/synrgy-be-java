package week2_1;

import java.util.Scanner;

public class Playground2 {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        System.out.print("Masukan angka (0 untuk keluar): ");
        int n = kb.nextInt();

        /*
        while (n != 0) {
            System.out.println("n = " + n);
            System.out.print("Masukan angka (0 untuk keluar): ");
            n = kb.nextInt();
        }
        System.out.println("Sudah keluar dari while loop");
        */

        while (true) {
            System.out.println("Masukan angka 0 || 25 || 30 untuk keluar");
            n = kb.nextInt();

            if (n == 0 || n == 25) {
                break;
            }else if (n == 30) {
                break;
            }

        }
        kb.close();
        System.out.println("sudah keluar dari while loop");
    }
}
