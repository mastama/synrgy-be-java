package week2_1;

public class PgFoorLoop {
    public static void main(String[] args) {

        /*
        int i = 0;
        for (i = 0; i < 10; i++) {
            if (i == 5) continue;

            System.out.println("Print 1-10 kecuali 5, " + i);
        }
        */

        int[] nilaiUlangan = {80, 88, 78 ,90};
        double totalNilai = 0;
        double rataNilai = 0;

        for (int i = 0; i < nilaiUlangan.length; i++) {
            totalNilai = totalNilai + nilaiUlangan[i];
        }

        rataNilai = totalNilai / nilaiUlangan.length;
        System.out.println("Total nilai = " + totalNilai);
        System.out.println("Rata-rata nilai = " + totalNilai/nilaiUlangan.length);

        if (rataNilai > 80) {
            System.out.println("Lulus dengan baik");
        }else if (rataNilai > 70) {
            System.out.println("Lulus biasa aja");
        }else if (rataNilai > 60) {
            System.out.println("Hampir tidak lulus");
        }else {
            System.out.println("TIdak Lulus");
        }
    }
}