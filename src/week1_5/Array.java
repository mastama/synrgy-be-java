package week1_5;

public class Array {
    public static void main(String[] args) {

        int[] value = new int[100]; // 0-99
        value[0] = 1;
        value[1] = 1;
        value[2] = 3;
        value[99] = 10;

        System.out.println("value[99] = " + value[99]);
        System.out.println("value[77] = " + value[77]);
//        System.out.println("value[100] = " + value[100]);

        int[] x = {30, 40, 50, 7, 8, 9};
        int[] y = x;
        y[3] = 10;
        System.out.println("nilai x[3] = " + x[3]);
    }
}
