package week1_5.pr;

import java.util.Scanner;

public class Pr1 {
    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);

        /*
        // Soal no 1
        System.out.print("Enter decimal #1: ");
        double n1 = kb.nextDouble();
        System.out.print("Enter decimal #2: ");
        double n2 = kb.nextDouble();

        if(n1 == n2) {
            System.out.println("n1 sama dengan n2");
        }else {
            System.out.println("n1 tidak sama dengan n2");
        }


        // Soal no2
        System.out.print("Enter number: ");
        int number = kb.nextInt();


        if (number % 2 == 0) {
            System.out.println(number + " adalah genap");
        }else {
            System.out.println(number + " adalah ganjil");
        }

         */

        // Soal no3
        System.out.print("Enter n1: ");
        int r1 = kb.nextInt();
        System.out.print("Enter n2: ");
        int r2 = kb.nextInt();
        System.out.print("Enter n3: ");
        int r3 = kb.nextInt();
        kb.close();

        if (r1 > r2 && r1 > r3) {
            System.out.println("r1 terbesar dan nilainya = " + r1);
        } else {
            if (r2 > r3) {
                System.out.println("r2 terbesar dan nilainya = " + r2);
            } else if (r1 == r2) {
                System.out.println("nilai r1 dan r2 sama besar");
            } else if (r2 == r3) {
                System.out.println("nilai r2 dan r3 sama besar");
            } else if ((r1 == r2) && (r1 == r3)) {
                System.out.println("nilai n1, n2, n3 sama besar");
            } else {
                System.out.println("r3 terbesar dan nilainya = " + r3);
            }
        }
    }
}
