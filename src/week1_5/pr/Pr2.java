package week1_5.pr;

import java.util.Scanner;

public class Pr2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.print("Masukkan satu huruf: ");
        /*
        char huruf = kb.nextLine().charAt(0);

        if (huruf == 'a' || huruf == 'A') {
            System.out.println(huruf + " adalah vokal");
        }else if (huruf == 'i' || huruf == 'I') {
            System.out.println(huruf + " adalah vokal");
        }else if (huruf == 'u' || huruf == 'U') {
            System.out.println(huruf + " adalah vokal");
        }else if (huruf == 'e' || huruf == 'E') {
            System.out.println(huruf + " adalah vokal");
        }else if (huruf == 'o' || huruf == 'O') {
            System.out.println(huruf + " adalah vokal");
        }else if (huruf > '0' && huruf < '9') {
            System.out.println(huruf + " bukan huruf");
        }else {
            System.out.println(huruf + " adalah konsonan");
        }
         */

        String huruf = kb.nextLine();

        if (huruf.equalsIgnoreCase("a") || huruf.equalsIgnoreCase("i") ||
            huruf.equalsIgnoreCase("u") || huruf.equalsIgnoreCase("e") ||
            huruf.equalsIgnoreCase("o")) {
            System.out.println(huruf + " adalah vokal");
        }else {
            System.out.println(huruf + " adalah konsonan");
        }
    }
}
